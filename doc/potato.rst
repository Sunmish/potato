potato package
==============

Submodules
----------

potato.fitsutils module
-----------------------

.. automodule:: potato.fitsutils
   :members:
   :undoc-members:
   :show-inheritance:

potato.msutils module
---------------------

.. automodule:: potato.msutils
   :members:
   :undoc-members:
   :show-inheritance:

potato.peel module
------------------

.. automodule:: potato.peel
   :members:
   :undoc-members:
   :show-inheritance:

potato.wrappers module
----------------------

.. automodule:: potato.wrappers
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: potato
   :members:
   :undoc-members:
   :show-inheritance:
