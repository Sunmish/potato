Installation
============

From GitLab:

.. code-block:: console

    pip install git+https://gitlab.com/Sunmish/potato.git

From local clone:

.. code-block:: console

    # After git clone
    cd PotatoPeel
    pip install .

Optional dependencies
---------------------
Numba can optionally be used to speed some of the visibility computations.

To install numba, run:

From GitLab:

.. code-block:: console

    pip install git+https://gitlab.com/Sunmish/potato.git[numba]

From local clone:

.. code-block:: console

    # After git clone
    cd PotatoPeel
    pip install .[numba]

To install the dependencies for the docs, run:

From GitLab:

.. code-block:: console

    pip install git+https://gitlab.com/Sunmish/potato.git[docs]

From local clone:

.. code-block:: console

    # After git clone
    cd PotatoPeel
    pip install .[docs]