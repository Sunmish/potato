
sphinx_rtd_theme==1.0.0
sphinx_autodoc_defaultargs==0.1.2
sphinx_autodoc_typehints==1.19.2
readthedocs-sphinx-search==0.1.1
sphinx>=5.1.1
