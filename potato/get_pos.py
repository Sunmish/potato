#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Get the position of an object in a measurement set.
"""
from pathlib import Path

import numpy as np
from astropy import units as u
from astropy.coordinates import (
    EarthLocation,
    SkyCoord,
    get_body,
    solar_system_ephemeris,
    get_sun
)
from astropy.time import Time
from casacore.tables import table

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def position_from_ms(
    ms_path: Path,
    obj_name: str,
    do_mean: bool = False,

) -> SkyCoord:
    # Get the time of the observation
    with table(str(ms_path), ack=False) as tab:
        logger.info(f"Reading {ms_path} for time information...")
        if do_mean:
            mean_mjd = np.mean(tab.getcol("TIME"), axis=0)/24/3600.
            logger.debug("Mean MJD time: {} d".format(mean_mjd))
            times = Time(mean_mjd, format="mjd")
        else:
            times = Time(tab.getcol("TIME") * u.s, format="mjd")

    # Get the position of the object
    if obj_name.lower() == "sun":
        coords = get_sun(times)

    else:
        # Get the position of the observatory
        with table(str(ms_path / "ANTENNA"), ack=False) as tab:
            logger.info(f"Reading {ms_path / 'ANTENNA'} for position information...")
            pos = EarthLocation.from_geocentric(
                *tab.getcol("POSITION")[0] * u.m  # First antenna is fine
            )

    
            logger.info("Converting to ICRS...")
            with solar_system_ephemeris.set("builtin"):
                coords = get_body(obj_name, times, pos).icrs
                coords = SkyCoord(coords.ra, coords.dec, frame="icrs")

    return coords

def parse_coords(
        ms_path: Path,
        obj_name: str,
        coords: SkyCoord, 
        save: bool = False,
        style: str = "decimal",
    ):
    # Print the position for scraping output
    print(coords.to_string(style))

    if save:
        outf = ms_path.with_suffix(f".{obj_name}.pos.txt")
        with open(outf, "w") as f:
            f.write("\n".join(coords.to_string()))
        logger.info(f"Saved position to {outf}")

def main(
    ms_path: Path,
    obj_name: str,
    do_mean: bool = False,
    save: bool = False,
    style: str = "decimal",
):
    coords = position_from_ms(ms_path, obj_name, do_mean=do_mean)
    parse_coords(ms_path, obj_name, coords, save=save, style=style)

def cli():
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("ms_path", type=str, help="Path to the measurement set.")
    parser.add_argument("obj_name", type=str, help="Name of the object.")
    parser.add_argument("--mean", action="store_true", help="Get the mean position.")
    parser.add_argument(
        "--save", action="store_true", help="Save the position to a file."
    )
    parser.add_argument(
        "--style", type=str, default="decimal", help="Style of the output."
    )
    args = parser.parse_args()
    main(
        Path(args.ms_path),
        args.obj_name,
        do_mean=args.mean,
        save=args.save,
        style=args.style,
    )


if __name__ == "__main__":
    cli()