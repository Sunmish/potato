#! /usr/bin/env python

"""
Write custom configuration files.
"""

CONFIG = """
[default]
scale = {image_scale}
size = {image_size}
briggs = {image_briggs}
mgain = {image_mgain}
channels = {image_channels}
niter = {image_niter}
nmiter = {image_nmiter}
multiscale = {image_multiscale}
multiscaleScaleBias = {image_msbias}
multiscaleMaxScales = {image_msmaxscales}
autoMask = {image_automask}
autoThreshold = {image_autothreshold}
noNegative = {image_nonegative}
minuvL = {image_minuvl}

[image]
scale = {image_scale}
size = {image_size}
briggs = {image_briggs}
mgain = {image_mgain}
channels = {image_channels}
niter = {image_niter}
nmiter = {image_nmiter}
multiscale = {image_multiscale}
multiscaleScaleBias = {image_msbias}
multiscaleMaxScales = {image_msmaxscales}
autoMask = {image_automask}
autoThreshold = {image_autothreshold}
noNegative = {image_nonegative}
minuvL = {image_minuvl}

[peel]
scale = {peel_scale}
size = {peel_size}
briggs = {peel_briggs}
mgain = {peel_mgain}
channels = {peel_channels}
niter = {peel_niter}
nmiter = {peel_nmiter}
multiscale = {peel_multiscale}
multiscaleScaleBias = {peel_msbias}
multiscaleMaxScales = {peel_msmaxscales}
autoMask = {peel_automask}
autoThreshold = {peel_autothreshold}
noNegative = {peel_nonegative}
minuvL = {peel_minuvl}
"""

IMAGE_SETTINGS={
    "scale": 0.0045,
    "size": 8000,
    "briggs": 0.0,
    "mgain": 0.8,
    "channels": 4,
    "niter": 50000,
    "nmiter": 3,
    "multiscale": False,
    "msbias": 0.7,
    "msmaxscales": 5,
    "automask": 3,
    "autothreshold": 1,
    "nonegative": False,
    "minuvl": 0.0
}

PEEL_SETTINGS={
    "scale": 0.0045,
    "size": 1000,
    "briggs": 0.0,
    "mgain": 0.8,
    "channels": 8,
    "niter": 20000,
    "nmiter": 7,
    "multiscale": True,
    "msbias": 0.7,
    "msmaxscales": 5,
    "automask": 3,
    "autothreshold": 1,
    "nonegative": False,
    "minuvl": 0.0
}

def write_config(image_settings, peel_settings, outname):

    image_settings = {**IMAGE_SETTINGS, **image_settings}
    peel_settings = {**PEEL_SETTINGS, **peel_settings}


    config = CONFIG.format(
        image_scale=image_settings["scale"],
        image_size=image_settings["size"],
        image_briggs=image_settings["briggs"],
        image_mgain=image_settings["mgain"],
        image_channels=image_settings["channels"],
        image_niter=image_settings["niter"],
        image_nmiter=image_settings["nmiter"],
        image_multiscale=image_settings["multiscale"],
        image_msbias=image_settings["msbias"],
        image_msmaxscales=image_settings["msmaxscales"],
        image_automask=image_settings["automask"],
        image_autothreshold=image_settings["autothreshold"],
        image_nonegative=image_settings["nonegative"],
        image_minuvl=image_settings["minuvl"],
        peel_scale=peel_settings["scale"],
        peel_size=peel_settings["size"],
        peel_briggs=peel_settings["briggs"],
        peel_mgain=peel_settings["mgain"],
        peel_channels=peel_settings["channels"],
        peel_niter=peel_settings["niter"],
        peel_nmiter=peel_settings["nmiter"],
        peel_multiscale=peel_settings["multiscale"],
        peel_msbias=peel_settings["msbias"],
        peel_msmaxscales=peel_settings["msmaxscales"],
        peel_automask=peel_settings["automask"],
        peel_autothreshold=peel_settings["autothreshold"],
        peel_nonegative=peel_settings["nonegative"],
        peel_minuvl=peel_settings["minuvl"],
    )

    with open(outname, "w+") as f:
        f.write(config)
    
    return

