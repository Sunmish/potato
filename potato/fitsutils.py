#! /usr/bin/env python

"""A collection of utilities for FITS images.

General utilies related to creation of FITS images, editing FITS images, and
scraping certain information from FITS images.

"""

from typing import Tuple, List, Optional, Union

import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
from astropy.stats import sigma_clip
from astropy.coordinates import SkyCoord
from astropy import units as u
from regions import Regions

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def norm_method(arr : np.ndarray, point : Tuple[np.ndarray]) -> np.ndarray:
    """Convience function for ``np.linalg.norm`` between points and an array.
    
    Args:
        arr: Input array.
        point: Input x,y points as tuple.
        
    Returns:
        Vector norm between ``point`` and ``array``.
        
    """
    point = np.asarray(point)
    return np.linalg.norm(np.array(np.indices(arr.shape, sparse=True), dtype=object)-point)


def make_template_image( 
    crval : Tuple[float, float], 
    npix : int, 
    cdelt : float,
    outname : Optional[str] = None) -> fits.HDUList:
    """Make a template image for creating, e.g. CLEAN masks.

    Args:
        crval:
            CRVAL1 and CRVAL2 values for template image.
        npix:
            Image size (NAXIS1 and NAXI2).
        cdelt:
            Absolute coordinate increment. ``CRVAL1 = -cdelt```, ``CRVAL2 = cdelt``.
        outname:
            If supplied, the template image will be written to disk as well.

    Returns:
        A FITS HDU.
    

    """

    crpix = npix // 2 - 1
    hdu = fits.PrimaryHDU()
    data = np.full((npix, npix), 0.)
    hdu.data = data

    hdu.header["CTYPE1"] = "RA---SIN"
    hdu.header["CTYPE2"] = "DEC--SIN"
    hdu.header["CRVAL1"] = crval[0]
    hdu.header["CRVAL2"] = crval[1]
    hdu.header["CDELT1"] = -cdelt
    hdu.header["CDELT2"] = cdelt
    hdu.header["CRPIX1"] = crpix
    hdu.header["CRPIX2"] = crpix

    if outname is not None:
        fits.writeto(outname, hdu.data, hdu.header, overwrite=True)
    
    return hdu


def ds9_mask(template : Union[str, fits.PrimaryHDU],
    region_file : str) -> Tuple[np.ndarray, fits.Header]:
    """Make a FITS mask based on a DS9 region file.
    
    Args: 
        template:
            Filename for template FITS image.
        region_file:
            Filename for DS9 region file.
    
    Returns:
        Mask FITS array.

    """

    if isinstance(template, str):
        image = fits.open(template)[0]
    else:
        image = template

    wcs = WCS(image.header).celestial
    indices = np.indices((image.header["NAXIS1"], image.header["NAXIS2"]))
    indices_x = indices[0]
    indices_y = indices[1]
    ra, dec = wcs.all_pix2world(indices_x, indices_y, 0)
    skycoords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)

    mask_image = np.full(
        (image.header["NAXIS1"], image.header["NAXIS2"]), 0
    )

    regions = Regions.read(region_file, format="ds9")
    
    for region in regions:

        mask = region.contains(skycoords, wcs)
        mask_image[indices_y[mask], indices_x[mask]] = 1

    if image.header["NAXIS"] > 2:
        mask_data = image.data.copy()
        mask_data[..., :, :] = mask_image
        mask_image = mask_data

    return mask_image, image.header    


def aperture_mask(template : str,
    coords : Tuple[float, float],
    radius : float) -> Tuple[np.ndarray, fits.Header]:
    """Make a FITS mask with an aperture.
    
    Args: 
        template:
            Filename for template FITS image.
        coords:
            (RA, DEC) coordinates in degrees for mask centre.
        radius:
            Radius of circular mask, centred on ``coords``.
    
    Returns:
        Mask FITS array.
    
    """

    if isinstance(template, str):
        image = fits.open(template)[0]
    else:
        image = template

    if "CDELT2" in image.header:
        cdelt = image.header["CDELT2"]
    else:
        cdelt = image.header["CD2_2"]  # ew

    mask_image = image.data.copy()
    mask_image[:] = 0
    wcs = WCS(image.header).celestial

    y, x =  wcs.all_world2pix(coords[0], coords[1], 0)
    dist = norm_method(mask_image, (x, y))
    cond = dist < radius/abs(cdelt)
    mask_image[np.where(cond)] = 1

    return mask_image, image.header


def make_fits_mask(template : str, 
    coords : Optional[Tuple[float, float]] = None, 
    radius : Optional[float] = None, 
    region_file : Optional[str] = None,
    outname : Optional[str] = None) -> str:
    """Make a FITS mask to use in WSClean.

    Args:
        template:
            Filename for template FITS image.
        coords:
            (RA, DEC) coordinates in degrees for mask centre.
        radius:
            Radius of circular mask, centred on ``coords``.
        region_file:
            Filename for DS9 region file.

        outname:
            Output filename. If not supplied, ``-mask.fits`` replaces ``.fits`` in
            the supplied ``template``.
        
    Returns:
        Mask FITS file name.


    """

    if not outname:
        outname = template.replace(".fits", "-mask.fits")
    if outname == template:
        raise RuntimeError("Output mask name and input FITS should be different!")

    if region_file is not None:
        logger.info("Making mask from region file: {}".format(
                region_file
            )
        )
        mask_image, header = ds9_mask(template, region_file)
    elif coords is not None and radius is not None:
        logger.info("Making mask from input coordinates: {}, {} (radius = {})".format(
            coords[0], coords[1], radius
        ))
        mask_image, header = aperture_mask(template, coords, radius)
    else:
        raise RuntimeError("DS9 region file or coordinates and radius required.")
    
    fits.writeto(outname, mask_image, header, overwrite=True)

    return outname


def get_bpp(bmaj : float, bmin : float, cd : float):
    """Get integrated beam area in pixel space."""
    return (np.pi*bmaj*bmin) / ((4.*cd**2)*np.log(2.))


def get_source_flux(fitsimage : str, coords : Tuple[float, float], radius : float, sigma : float = 3.):
    """Extract (rough) flux density and SNR for a given aperture.

    Args:
        fitsimage:
            Filename for FITS image.
        coords:
            (RA, DEC) coordinates in degrees for aperture centre.
        radius:
            Radius of aperture, centred on ``coords``.
        sigma:
            Pixels < `sigma`*stddev. are clipped.

        
    Returns:
        
    """

    if isinstance(fitsimage, str):
        f = fits.open(fitsimage)[0]
    else:
        f = fitsimage
    if "CDELT2" in f.header:
        cdelt = f.header["CDELT2"]
    else:
        cdelt = f.header["CD2_2"]  # ew
    w = WCS(f.header).celestial
    y, x = w.all_world2pix(coords[0], coords[1], 0)
    dist = norm_method(np.squeeze(f.data), (x, y))
    cond = dist < radius/abs(cdelt)

    aperture = np.where(cond)

    std = np.nanstd(sigma_clip(np.squeeze(f.data), sigma=3))
    data_aperture = np.squeeze(f.data)[aperture]
    data_aperture[data_aperture < sigma*std] = np.nan

    bpp = get_bpp(
        bmaj=f.header["BMAJ"],
        bmin=f.header["BMIN"],
        cd=cdelt
    )

    flux_int = np.nansum(data_aperture) / bpp
    flux_peak = np.nanmax(data_aperture)

    return std, flux_int, flux_peak


def get_max(fitsimage : str) -> float:
    return np.nanmax(np.squeeze(fits.getdata(fitsimage)))
def get_sum(fitsimage : str) -> float:
    return np.nansum(np.squeeze(fits.getdata(fitsimage)))
def get_std(fitsimage : str) -> float:
    return np.nanstd(np.squeeze(fits.getdata(fitsimage)))



