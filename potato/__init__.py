from importlib import resources
_DEFAULT_CONFIG = "defaults.conf"

def get_data(resource: str):
    return resources.path("potato.data", resource)