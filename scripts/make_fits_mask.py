#! /usr/bin/env python

from argparse import ArgumentParser

from potato.fitsutils import make_template_image, make_fits_mask

def getargs():

    ps = ArgumentParser()
    ps.add_argument("ra", type=float)
    ps.add_argument("dec", type=float)
    ps.add_argument("npix", type=int)
    ps.add_argument("cdelt", type=float)
    ps.add_argument("radius", type=float)
    ps.add_argument("--mask_ra", default=None, type=float)
    ps.add_argument("--mask_dec", default=None, type=float)

    ps.add_argument("-o", "--outname", default="mask.fits")

    args = ps.parse_args()
    return args


def make_mask(args):

    hdu = make_template_image(
        crval=(args.ra, args.dec),
        npix=args.npix,
        cdelt=args.cdelt,
        outname=None
    )

    if not args.mask_ra:
        args.mask_ra = args.ra
    if not args.mask_dec:
        args.mask_dec = args.dec

    make_fits_mask(
        template=hdu,
        coords=(args.mask_ra, args.mask_dec),
        radius=args.radius,
        outname=args.outname
    )


def cli():
    make_mask(getargs())

if __name__ == "__main__":
    cli()