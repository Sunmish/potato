#! /usr/bin/env python

from argparse import ArgumentParser

import shutil
import os

from potato.wrappers import split_wrapper



def get_args():

    ps = ArgumentParser("Split out DATA column of an MS.")
    ps.add_argument("ms")
    ps.add_argument("-c", "-o", "--overwrite", action="store_true")
    ps.add_argument("--outname", default=None)
    return ps.parse_args()


def split(ms, outname=None, overwrite=False):

    if outname is None:
        temp_outname = ms + ".1"
    else:
        temp_outname = outname

    if os.path.exists(temp_outname) and overwrite:
        shutil.rmtree(temp_outname)

    split_wrapper(ms, temp_outname)

    if outname is None or outname == ms:
        shutil.rmtree(ms)
        shutil.move(temp_outname, ms)

    

def cli(args):

    split(args.ms, args.outname, args.overwrite)


if __name__ == "__main__":
    cli(get_args())
