#! /usr/bin/env python

from argparse import ArgumentParser

from potato.msutils import do_rotate

def getargs():

    ps = ArgumentParser(description="Phase rotate column(s) of MeasurementSet.")

    ps.add_argument("ms", help="MeasurementSet.")
    ps.add_argument("ra", type=float, help="RA direction. Decimal degrees.")
    ps.add_argument("dec", type=float, help="DEC direction. Decimal degrees.")
    ps.add_argument("-c", "--columns", type=str, nargs="*", default=["DATA", "MODEL_DATA", "CORRECTED_DATA"])

    args = ps.parse_args()
    return args.ms, args.ra, args.dec, args.columns

def rotate(ms, ra, dec, columns):
    """
    """

    do_rotate(msname=ms, ra=ra, dec=dec, datacolumn=columns)

def cli():
    rotate(*getargs())

if __name__ == "__main__":
    cli()

